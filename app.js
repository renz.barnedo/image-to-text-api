const express = require('express');
const fs = require('fs');
const multer = require('multer');

const app = express();

const { createWorker } = require('tesseract.js');

const worker = createWorker({
  logger: (progress) => {
    console.log(progress);
  },
});

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage: storage }).single('avatar');

app.set('view engine', 'ejs');

app.get('/', (req, res, next) => {
  res.render('index');
});

app.post('/upload', (req, res) => {
  upload(req, res, (err) => {
    fs.readFile(`./uploads/${req.file.originalname}`, async (err, data) => {
      if (err) {
        return console.log('Error', err);
      }

      await worker.load();
      await worker.loadLanguage('eng');
      await worker.initialize('eng');
      const {
        data: { text },
      } = await worker.recognize(data, 'eng', {
        logger: (log) => {
          console.log(log);
        },
      });
      console.log(text);
      res.send(text);
      await worker.terminate();
    });
  });
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Server running on port: ${PORT}`);
});
